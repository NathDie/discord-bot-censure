const Discord = require('discord.js');
const bot = new Discord.Client();

bot.on('ready', function() {
    console.log("I'm ready");
})

// Bot need delete message permission
bot.on('message', message => {
    if (message.author.id === "CENSURE_ID_USER") {
        message.delete();
    }
})

bot.login("BOT_TOKEN");